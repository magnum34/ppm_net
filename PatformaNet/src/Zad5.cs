﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PatformaNet.src
{
    class Zad5
    {
        public Zad5()
        {

        }
        public void Make()
        {
            try
            {


                using (StreamReader sr = new StreamReader("i100.ppm"))
                using (StreamWriter outputFile = new StreamWriter("zad5.txt", false))
                {
                    String name, size, line;
                    int saturation;
                    name = sr.ReadLine();
                    Console.WriteLine("Nazwa pliku :" + name);


                    size = sr.ReadLine();
                    Console.WriteLine(size);

                    line = sr.ReadLine();
                    saturation = int.Parse(line);
                    Console.WriteLine(saturation);
                    string content = sr.ReadToEnd();
                    string[] rows = content.Split('\n');
                    string[][] matrix = new string[rows.Length - 1][];
                    for (int i = 0; i < rows.Length - 1; i++)
                    {
                        matrix[i] = rows[i].Split(' ');
                    }

                    int rowLength = matrix[0].Length;
                    for (int i = (rowLength - 1); i != -1; i--)
                    {

                        for (int j = (matrix.Length - 2); j != -1; j--)
                        {

                            outputFile.Write(matrix[j][i - 2] + " ");
                            outputFile.Write(matrix[j][i - 1] + " ");
                            outputFile.Write(matrix[j][i] + " ");

                        }
                        i--;
                        --i;
                        outputFile.WriteLine();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }
    }
}
