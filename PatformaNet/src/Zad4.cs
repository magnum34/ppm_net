﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PatformaNet.src
{
    class Zad4
    {
        public Zad4()
        {

        }
        public void make()
        {
            try
            {

                String name, size, line, saturation;

                using (StreamReader sr = new StreamReader("i100.ppm"))
                using (StreamWriter outputFile = new StreamWriter("zad4.txt", false))
                {
                    name = sr.ReadLine();
                    Console.WriteLine("Nazwa pliku :" + name);
                    size = sr.ReadLine();
                    Console.WriteLine("Size {0}", size);
                    saturation = sr.ReadLine();
                    Console.WriteLine("Saturation {0}", saturation);
                    while ((line = sr.ReadLine()) != null)
                    {

                        string[] stringArray = line.Split(' ');

                        //matrix.Add(stringArray);

                        int[] pixelsArray = Array.ConvertAll(stringArray, delegate(string s) { return int.Parse(s); });


                        for (int i = 0; i < pixelsArray.Length; i++)
                        {
                            outputFile.Write((pixelsArray[i] + pixelsArray[i + 1] + pixelsArray[i + 2]) / 3 + " ");

                            i++;
                            i++;
                        }
                        outputFile.WriteLine();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }
    }
}
