﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace PatformaNet.src
{
    class Zad1
    {
        public Zad1()
        {

        }
        public void make()
        {
            try
            {

                String name, size, line;
                int saturation = 255;
                
                using (StreamReader sr = new StreamReader("i100.ppm"))
                using (StreamWriter outputFile = new StreamWriter("zad1.txt", false))
                {
                    name = sr.ReadLine();
                    Console.WriteLine("Nazwa pliku :" + name);
                    while ((line = sr.ReadLine()) != null)
                    {

                        string[] stringArray = line.Split(' ');

                        //matrix.Add(stringArray);

                        int[] pixelsArray = Array.ConvertAll(stringArray, delegate(string s) { return int.Parse(s); });


                        foreach (int pixel in pixelsArray)
                        {

                            outputFile.Write((saturation - pixel) + " ");
                        }

                        outputFile.WriteLine();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

           
        }
    }
}
